//
//  YCAlbumModel.h
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCAlbumModel : NSObject
@property(strong,nonatomic)NSString *album_id;
@property(assign,nonatomic)NSString *owner_id;
@property(assign,nonatomic)NSInteger countPhotos;
@property(assign,nonatomic)NSString *thumb_id;
@property(strong,nonatomic)NSString *title;
@property(strong,nonatomic)NSArray *photosArray;

-(id)initWithRequest:(NSDictionary*) request;
    

@end
