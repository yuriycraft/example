//
//  YCPhotoViewController.m
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "MWPhotoBrowser.h"
#import "VKSdk.h"
#import "YCAlbumModel.h"
#import "YCAlbumTableViewCell.h"
#import "YCPhotoCollectionViewCell.h"
#import "YCPhotoModel.h"
#import "YCPhotoTableViewCell.h"
#import "YCPhotoViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIScrollView+InfiniteScroll.h>


static NSString *kPhotoCellId = @"photoCell";
static NSString *kAlbumCellId = @"albumCell";

#define COUNTALBUMS 5
#define COUNTPHOTOS 10

@interface YCPhotoViewController () <
UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegate, MWPhotoBrowserDelegate,
UICollectionViewDelegateFlowLayout
#warning unComment
,YCPhotoViewControllerDelegate,YCPhotoCollectionViewCellDelegate>
//>


@property(weak, nonatomic) IBOutlet UITableView *tableView;

@property(strong, nonatomic) NSMutableArray *albumsArray;

@property(strong, nonatomic) NSMutableArray *collectionPhotosArray;
@property(assign, nonatomic) NSInteger indexPosition;
@property(weak, nonatomic) YCPhotoCollectionViewCell *cellSizing;

@property(weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property(strong, nonatomic) NSMutableArray *requestsArray;

@property(assign, nonatomic) BOOL isOverCountAlbum;

@property(assign, nonatomic) BOOL isAlbumLoad;
@property(assign, nonatomic) BOOL isPhotosLoad;
@property(assign, nonatomic) BOOL isAllPhotoAlbum;
@property(strong, nonatomic) NSMutableArray *pagePhotoIndexArray;

#warning unComment
@property(nonatomic, strong, readwrite) NSMutableArray *seletedImageArray;

@end

@implementation YCPhotoViewController {
    
    NSInteger _pageIndexAlbums;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _isOverCountAlbum = NO;
    _isPhotosLoad = NO;
    _isAllPhotoAlbum = NO;
    _pageIndexAlbums = 0;
    
    //
    self.ownerId = @"-10290"; //[[NSUserDefaults standardUserDefaults]
    //objectForKey:@"userId"];
    
    self.collectionPhotosArray = [NSMutableArray array];
    
    self.requestsArray = [NSMutableArray array];
    self.pagePhotoIndexArray = [NSMutableArray array];
    

 
    
    
    self.tableView.tableHeaderView = [[UIView alloc]
                                      initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width,
                                                               0.01f)];
    // change indicator view style to white
    self.tableView.infiniteScrollIndicatorStyle = UIActivityIndicatorViewStyleGray;
    [self getListAlbums];
    
    // setup infinite scroll

    
#warning 
    self.maxNumberOfSelections = 10 ;
        self.seletedImageArray = [NSMutableArray array];
    
}

#pragma mark - Get data from server

//Получение спискка альбомов
- (void)getListAlbums {
           [self.tableView addInfiniteScrollWithHandler:^(UITableView* tableView) {
                  }];
    self.isAlbumLoad = YES;
    
    NSInteger preloadOffset = _pageIndexAlbums * COUNTALBUMS;
    [self.requestsArray removeAllObjects];

    VKRequest *getAlbums =
    [VKRequest requestWithMethod:@"photos.getAlbums"
                   andParameters:@{
                                   @"access_token" : [[NSUserDefaults standardUserDefaults]
                                                      objectForKey:@"access_token"],
                                   VK_API_OWNER_ID : self.ownerId,
                                   @"offset" : [NSNumber numberWithInteger:preloadOffset],
                                   @"count" : [NSNumber numberWithInteger:COUNTALBUMS],
                                   @"need_system" : @"1"
                                   }
                   andHttpMethod:@"GET"];
    
    [getAlbums executeWithResultBlock:^(VKResponse *response) {
    
        NSArray *arrayAlbums = response.json[@"items"];
        
        if (arrayAlbums.count == 0) {
            
            _isOverCountAlbum = YES;
            
        } else {
            
            _isOverCountAlbum = NO;
        }
        
        if (_pageIndexAlbums == 0) {
            self.albumsArray = [NSMutableArray new];
            [self createAllPhotoAlbum];
            
            //Ставим запрос для всех фотографий первым в список
            [self.requestsArray addObject:[self createRequestAllPhotos]];
        }
        
        _pageIndexAlbums++;
        
        
               
        for (NSInteger i = 0; i < arrayAlbums.count; i++) {
            
            NSDictionary *albumDict = arrayAlbums[i];
            
            YCAlbumModel *album = [[YCAlbumModel alloc] initWithRequest:albumDict];
            
            [self.albumsArray addObject:album];
            
            //Ставим запрос в список
            [self.requestsArray
             addObject:[self createRequestWithAlbumId:album.album_id]];
            
            
         
                //
                // fetch your data here, can be async operation,
                // just make sure to call finishInfiniteScroll in the end
                //
                
                
                NSArray<NSIndexPath *> * indexPaths; // index paths of updated rows
                
                // make sure to update tableView before calling -finishInfiniteScroll
//                [tableView beginUpdates];
//                [tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
//                [tableView endUpdates];
           
         

            
           // [self.tableView reloadData];
        }
               
        self.isAlbumLoad = NO;
        
        //запрашиваем фото для всех альбомов
        [self getPhotosWithRequestArray:self.requestsArray];
        
        NSLog(@"Fetch OK");
        [self.tableView reloadData];
        // finish infinite scroll animation
        [self.tableView finishInfiniteScroll];
        
        
    }
                           errorBlock:^(NSError *error) {
                               if (error.code != VK_API_ERROR) {
                                   [error.vkError.request repeat];
                               } else {
                                   NSLog(@"VK error: %@", error);
                                      [self.tableView finishInfiniteScroll];
                               }
                           }];
               
}

//Получение данных о фото
- (void)getPhotosWithRequestArray:(NSArray *)requestsArray {
    
    VKBatchRequest *batch =
    [[VKBatchRequest alloc] initWithRequestsArray:requestsArray];
    
    [batch executeWithResultBlock:^(NSArray *responses) {
        
        NSArray *res = responses;
        
        NSMutableArray *arrayPhoto = [NSMutableArray array];
        
        if( ! _isAllPhotoAlbum ){
            _isAllPhotoAlbum = YES;
            YCAlbumModel *allPhoto = self.albumsArray[0];
            
            VKResponse *photosResponse = res[0];
            
            allPhoto.countPhotos = [photosResponse.json[@"count"] integerValue];
            
            [self.albumsArray replaceObjectAtIndex:0 withObject:allPhoto];
        }
        for (int i = 0; i < res.count; i++) {
            VKResponse *photosResponse = res[i];
            
            
            
            NSArray *phArray = photosResponse.json[@"items"];
            [arrayPhoto removeAllObjects];
            
            for (NSDictionary *photoDict in phArray) {
                
                YCPhotoModel *photo = [[YCPhotoModel alloc] initWithRequest:photoDict];
                photo.selectedStatus = @NO;
                
                [arrayPhoto addObject:photo];
            }
            
            [self.collectionPhotosArray addObject:arrayPhoto.mutableCopy];
            
            YCPhotoTableViewCell *tablePhotoCell = [self.tableView
                                                    cellForRowAtIndexPath:[NSIndexPath
                                                                           indexPathForRow:1
                                                                           inSection:self.collectionPhotosArray
                                                                           .count -
                                                                           1]];
          //  tablePhotoCell.pageIndexPhotos++;
            
            NSNumber *page = [NSNumber numberWithInteger:1];
            [self.pagePhotoIndexArray addObject:page];
          //  [tablePhotoCell setNeedsLayout];
            
          //
        }
        [self.tableView reloadData];
    }
                       errorBlock:^(NSError *error) {
                           
                           if (error.code != VK_API_ERROR) {
                               [error.vkError.request repeat];
                           } else {
                           }
                       }];
}

//Создание запроса фотографий для конкретного альбома
- (VKRequest *)createRequestWithAlbumId:(NSString *)albumId {
    
    VKRequest *getPhotos =
    [VKRequest requestWithMethod:@"photos.get"
                   andParameters:@{
                                   @"access_token" : [[NSUserDefaults standardUserDefaults]
                                                      objectForKey:@"access_token"],
                                   VK_API_OWNER_ID : self.ownerId,
                                   @"album_id" : albumId,
                                   @"count" : @"10",
                                   @"rev" : @"1"
                                   }
                   andHttpMethod:@"GET"];
    
    [getPhotos setPreferredLang:@"ru"];
    
    return getPhotos;
}

// получение фотографий при скроллинге
- (void)getMorePhotosWithAlbumId:(NSString *)albumId
             andIndexPathSection:(NSIndexPath *)indexPath {
    
    
    NSNumber *pageWithIndex = self.pagePhotoIndexArray[indexPath.section];
    NSInteger page = [pageWithIndex integerValue];
    
    YCPhotoTableViewCell *tablePhotoCell =
    [self.tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger preloadOffset = page * COUNTPHOTOS;
    self.isPhotosLoad = YES;
    
    VKRequest *getPhotos;
    //Запрос данных о фотографиях в альбоме
    if (albumId) {
        
        getPhotos = [VKRequest
                     requestWithMethod:@"photos.get"
                     andParameters:@{
                                     @"access_token" : [[NSUserDefaults standardUserDefaults]
                                                        objectForKey:@"access_token"],
                                     VK_API_OWNER_ID : self.ownerId,
                                     @"album_id" : albumId,
                                     @"offset" : [NSNumber numberWithInteger:preloadOffset],
                                     @"count" : [NSNumber numberWithInteger:COUNTPHOTOS],
                                     @"rev" : @"1"
                                     }
                     andHttpMethod:@"GET"];
        
        [getPhotos setPreferredLang:@"ru"];
        
    }
    
    //Запрос данных о фотографиях в альбоме ВСЕ ФОТО
    else if (indexPath.section == 0 && self.collectionPhotosArray.count > 0) {
        
        getPhotos = [VKRequest
                     requestWithMethod:@"photos.getAll"
                     andParameters:@{
                                     @"access_token" : [[NSUserDefaults standardUserDefaults]
                                                        objectForKey:@"access_token"],
                                     VK_API_OWNER_ID : self.ownerId,
                                     @"offset" : [NSNumber numberWithInteger:preloadOffset],
                                     @"count" : [NSNumber numberWithInteger:COUNTPHOTOS],
                                     @"no_service_albums" : @"0",
                                     @"rev" : @"1"
                                     }
                     andHttpMethod:@"GET"];
        
        [getPhotos setPreferredLang:@"ru"];
    }
    
    [getPhotos executeWithResultBlock:^(VKResponse *response) {
        
        NSMutableArray *arrayPhoto = [NSMutableArray array];
        
        NSArray *phArray = response.json[@"items"];
        
//        if (phArray.count == 0) {
//            
//            tablePhotoCell.isOverCountPhoto = YES;
//            
//        } else {
//            
//            tablePhotoCell.isOverCountPhoto = NO;
//        }
        
        [arrayPhoto removeAllObjects];
        
        for (NSDictionary *photoDict in phArray) {
            
            YCPhotoModel *photo = [[YCPhotoModel alloc] initWithRequest:photoDict];
            [arrayPhoto addObject:photo];
        }
        
        NSLog(@"ДО %lu",
              (unsigned long)[(NSMutableArray *)self.collectionPhotosArray[indexPath.section]
               count]);
        
        [self.collectionPhotosArray[indexPath.section]
         addObjectsFromArray:arrayPhoto];
        
       // tablePhotoCell.pageIndexPhotos++;
        
        NSNumber *pageWithIndex = self.pagePhotoIndexArray[indexPath.section];
        
        NSInteger page = [pageWithIndex integerValue] + 1;
        
        [self.pagePhotoIndexArray
         replaceObjectAtIndex:indexPath.section
         withObject:[NSNumber numberWithInteger:page]];
        
        NSLog(@" После %lu",
              (unsigned long)[(NSMutableArray *)self.collectionPhotosArray[indexPath.section]
               count]);
        
        [self.tableView reloadData];
        
        self.isPhotosLoad = NO;
        
    }
                           errorBlock:^(NSError *error) {
                               
                               if (error.code != VK_API_ERROR) {
                                   [error.vkError.request repeat];
                               } else {
                                   
                               }
                           }];
}

//Создание запроса для всех фотографий
- (VKRequest *)createRequestAllPhotos {
    
    VKRequest *getPhotos =
    [VKRequest requestWithMethod:@"photos.getAll"
                   andParameters:@{
                                   @"access_token" : [[NSUserDefaults standardUserDefaults]
                                                      objectForKey:@"access_token"],
                                   VK_API_OWNER_ID : self.ownerId,
                                   @"count" : [NSNumber numberWithInteger:COUNTPHOTOS],
                                   @"no_service_albums" : @"0",
                                   @"rev" : @"1"
                                   }
                   andHttpMethod:@"GET"];
    
    [getPhotos setPreferredLang:@"ru"];
    
    return getPhotos;
}

//Создаем новый альбом "Все фотографии"
- (void)createAllPhotoAlbum {
    
    YCAlbumModel *albumAllPhotos = [YCAlbumModel new];
    
    albumAllPhotos.title = @"Все фотографии";
    albumAllPhotos.album_id = nil;
    albumAllPhotos.owner_id = self.ownerId;
    [self.albumsArray addObject:albumAllPhotos];
}

#pragma mark -UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YCAlbumModel *albumModel = [self.albumsArray objectAtIndex:indexPath.section];
    if (indexPath.row == 0) {
        
        YCAlbumTableViewCell *cell = (YCAlbumTableViewCell *)[tableView
                                                              dequeueReusableCellWithIdentifier:kAlbumCellId];
        
        cell.albumNameLabel.text = albumModel.title;
//        if (albumModel.countPhotos > 0 ) {
            cell.photoCountLabel.text =
            [NSString stringWithFormat:@"%lu фотографий", (long)albumModel.countPhotos];
//        } else {
//            cell.photoCountLabel.text = @" ";
//        }
     //   }
        return cell;
        
    } else if (indexPath.row == 1) {
        
        YCPhotoTableViewCell *cell = (YCPhotoTableViewCell*)
        [tableView dequeueReusableCellWithIdentifier:kPhotoCellId
                                        forIndexPath:indexPath];
        
        if (self.collectionPhotosArray.count > 0 &&
            indexPath.section < self.collectionPhotosArray.count) {
            [cell setCollectionViewDataSourceDelegate:self
                                     indexPathSection:indexPath.section];
            NSArray *array = _collectionPhotosArray[indexPath.section];
         
            
//            if (array.count < albumModel.countPhotos) {
//                
//                cell.isOverCountPhoto = NO;
//             
//            }
            
        } else {
            [cell setCollectionViewDataSourceDelegate:nil
                                     indexPathSection:indexPath.section];
        }
        
        return cell;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.albumsArray.count;
}

#pragma mark - UITableViewDelegate Methods

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 65;
    } else if (indexPath.row == 1) {
        return 100;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.collectionPhotosArray.count > 0) {
        if (indexPath.row == 0) {
            self.indexPosition = indexPath.section;
            
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            
            browser.displayActionButton = NO;
            
            browser.enableGrid = NO;
            browser.startOnGrid = YES;
            
            [browser setCurrentPhotoIndex:0];
            
            [self.navigationController pushViewController:browser animated:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1 && [cell isKindOfClass:[YCPhotoTableViewCell class]]) {
        [[(YCPhotoTableViewCell *)cell myCollectionView] setTag:indexPath.section];
        
        if (self.collectionPhotosArray.count > 0 &&
            indexPath.section < self.collectionPhotosArray.count) {
            [(YCPhotoTableViewCell *)cell setCollectionViewDataSourceDelegate:self
                                     indexPathSection:indexPath.section];
            NSArray *array = _collectionPhotosArray[indexPath.section];
            
            
            //            if (array.count < albumModel.countPhotos) {
            //
            //                cell.isOverCountPhoto = NO;
            //
            //            }
            
        } else {
            [(YCPhotoTableViewCell *)cell setCollectionViewDataSourceDelegate:nil
                                     indexPathSection:indexPath.section];
        }

    }
    
    if (self.collectionPhotosArray.count > 0 &&
        indexPath.section < self.collectionPhotosArray.count) {
    }
    
    NSInteger lastSectionIndex;
    NSInteger lastRowIndex;
    
    lastSectionIndex = [tableView numberOfSections] - 2;
    lastRowIndex = [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    
    if ((indexPath.section == lastSectionIndex) &&
        (indexPath.row == lastRowIndex)) {
        // This is the last cell
        
        if (!_isOverCountAlbum && !_isAlbumLoad) {
            [self getListAlbums];
            NSLog(@"FETCH");
        }
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(YCPhotoCollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    
    return
    [(NSMutableArray *)self.collectionPhotosArray[collectionView.tag] count];
}

- (YCPhotoCollectionViewCell *)collectionView:
(YCPhotoCollectionView *)collectionView
                       cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YCPhotoCollectionViewCell *cell = [collectionView
                                       dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier
                                       forIndexPath:indexPath];
    
   
    YCPhotoModel *model =
    self.collectionPhotosArray[collectionView.tag][indexPath.item];


#warning
    cell.cellDelegate = self;
    
    
    
    
    
    cell.selectedStatus = model.selectedStatus;
    self.cellSizing = cell;
//    [cell.image sd_setImageWithURL:[NSURL URLWithString:model.photo_130]
//                         completed:^(UIImage *image, NSError *error,
//                                     SDImageCacheType cacheType, NSURL *imageURL){
//                             
//                         }];
    
    
#warning 
    //Select button block
    [cell buttonSelectBlock:^(NSNumber *seletedStatus) {
        
        model.selectedStatus = seletedStatus;
        
      //  model.index = indexPathRow;
        
       // assetsModel.imageAsset = _currentFetchResult[indexPathRow];
        
        if ([model.selectedStatus isEqual:@YES]) {
            
            [_seletedImageArray addObject:model];
          //replaceObjectAtIndex:index] withObject:model]];
       //     self.doneButton.enabled = YES;
            
        } else {
            
            [_seletedImageArray removeObject:model];
            
            if (!_seletedImageArray.count) {
                
       //         self.doneButton.enabled = NO;
            }
        }
        
        NSUInteger index = [_collectionPhotosArray[collectionView.tag] indexOfObject:model];
        
        [(NSMutableArray *)_collectionPhotosArray[collectionView.tag] replaceObjectAtIndex:index withObject:model];
    }];
    
    
    
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(YCPhotoCollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCPhotoModel *model =
    self.collectionPhotosArray[collectionView.tag][indexPath.item];
    
    [cell.image sd_setImageWithURL:[NSURL URLWithString:model.photo_130]
                         completed:^(UIImage *image, NSError *error,
                                     SDImageCacheType cacheType, NSURL *imageURL){
                             
                         }];

    
    if (self.collectionPhotosArray[indexPath.section]) {
        
        NSInteger lastItemIndex;
        
        if ([collectionView numberOfItemsInSection:0] > 11) {
            
            lastItemIndex = [collectionView numberOfItemsInSection:0] - 6;
        } else {
            
            if( [collectionView numberOfItemsInSection:0] > 3){
                
                lastItemIndex = [collectionView numberOfItemsInSection:0] - 3;
            }
            else {
                
            lastItemIndex = [collectionView numberOfItemsInSection:0] - 1;
            }
            }
        
        if ((indexPath.item == lastItemIndex)) {
            // This is the last cell
            NSIndexPath *indexP =
            [NSIndexPath indexPathForRow:1 inSection:collectionView.tag];
            
//            YCPhotoTableViewCell *tablePhotoCell =
//            [self.tableView cellForRowAtIndexPath:indexP];
            
            YCAlbumModel *album = self.albumsArray[collectionView.tag];
            
            if ((album.countPhotos > [(NSMutableArray*) self.collectionPhotosArray[indexP.section] count]) && !_isPhotosLoad) {
            
                if (album.album_id) {
                    
                    [self getMorePhotosWithAlbumId:album.album_id
                               andIndexPathSection:indexP];
                    
                } else if(collectionView.tag == 0) {
                    
                    [self getMorePhotosWithAlbumId:nil
                               andIndexPathSection:indexP];
                }
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = NO;
    browser.enableGrid = NO;
    self.indexPosition = collectionView.tag;
    
    [browser setCurrentPhotoIndex:indexPath.item];
    [self.navigationController pushViewController:browser animated:YES];
}

#pragma mark UICollectionViewFlowLayoutDelegate

- (CGSize)collectionView:(YCPhotoCollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    YCPhotoModel *model =
    self.collectionPhotosArray[collectionView.tag][indexPath.item];
    
    CGSize sizeImage = CGSizeMake(95, 95);
    CGFloat ratio = [model.height floatValue] / [model.width floatValue];
    
    if (ratio < 1.f) {
        
        sizeImage = CGSizeMake(130.f, 95.f);
        return sizeImage;
    } else if (ratio > 1.f) {
        
        sizeImage = CGSizeMake(75, 95.f);
        
        return sizeImage;
    } else if (ratio == 1.f) {
        
        sizeImage = CGSizeMake(95, 95.f);
    }
    
    return sizeImage;
}

#pragma mark -MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    
    return [(NSArray *)self.collectionPhotosArray[self.indexPosition] count];
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
               photoAtIndex:(NSUInteger)index {
    
    YCPhotoModel *model =
    
    self.collectionPhotosArray[self.indexPosition][index];
    
    MWPhoto *browserImage =
    [MWPhoto photoWithURL:[NSURL URLWithString:model.photo_604]];
    
    browserImage.caption = model.text;
    
    return browserImage;
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser
          thumbPhotoAtIndex:(NSUInteger)index {
    YCPhotoModel *model =
    
    self.collectionPhotosArray[self.indexPosition][index];
    
    MWPhoto *browserImage =
    [MWPhoto photoWithURL:[NSURL URLWithString:model.photo_604]];
    
    browserImage.caption = model.text;
    
    return browserImage;
}





#pragma mark -  YCPhotoCollectionViewCellDelegate;

- (void)setSelectedItemWithCell:(YCPhotoCollectionViewCell *)cell {
    
    if (cell.selectButton.selected) {
        
        cell.selectedStatus = @NO;
        
        cell.selectBlock(@NO);
        
    } else if (!cell.selectButton.selected) {
        
        if (self.seletedImageArray.count < self.maxNumberOfSelections) {
            
            cell.selectedStatus = @YES;
            
            cell.selectBlock(@YES);
        }
        else {
            
            cell.selectedStatus = @NO;
            
            cell.selectBlock(@NO);
            
    //        [self shakeAnimation:self.doneButton.badge];
        }
    }
}


@end
