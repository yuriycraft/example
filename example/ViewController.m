//
//  ViewController.m
//  example
//
//  Created by Dmitriy on 24.03.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*

 userId и access_token должны браться из:
 NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"];
 NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
 а не из sdk вконтакте, (при авторизации они уже созданы)
 
 при запросах к API вконтакте @"access_token" должен быть указан в передаваемых параметрах
 
 
 
*/


- (IBAction)presentAddPost:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"photoStoryboard" bundle:nil];
    UINavigationController *photoVC = [sb instantiateViewControllerWithIdentifier:@"photoNavigationViewController"];
    [self presentViewController:photoVC animated:YES completion:NULL];
}


/*
Чтобы открыть страницу пользователя/группы нужно передать данные и открыть след класс:
(в примере приложения его нет, нужно просто сделать по образцу и удостовериться, что соответствующие данные приходят и этот код закомментировать)


 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 
 // открыть страницу пользователя или группы
 GroupViewController *groupVC = (GroupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"groupVC"];
 groupVC.group_name = @"Имя группы" ;
 groupVC.group_id = @"id группы/пользователя" ; //id группы со знаком "-", например -34324325
 
 [self.navigationController pushViewController:groupVC animated:YES];
 */


/*
переходы на другие страницы пока выполнять не нужно, но нужно подготовить методы, которые будут выполняться по нажатии на соответствующий элемент
и передавать в этот метод данные, необходимые для открытия элемента

Например:

-(void)openPost:(NSDictionary *)data{
    
    
    
}

*/















@end
