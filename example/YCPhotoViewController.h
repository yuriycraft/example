//
//  YCPhotoViewController.h
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

#warning uncomment

@class YCPhotoViewController;

@protocol YCPhotoViewControllerDelegate <NSObject>

@optional

- (void)imagePicker:(YCPhotoViewController *)picker didFinishPickingMediaWithImageArray:(NSArray *)imageArray;

@end




@interface YCPhotoViewController : UIViewController

@property(strong, nonatomic) NSString* ownerId;

#warning unComment
@property(assign, nonatomic) BOOL isPicker;

@property(nonatomic, weak, readwrite) id <YCPhotoViewControllerDelegate> imagePickerDelegate;

@property(nonatomic , assign) NSInteger maxNumberOfSelections;
@end
