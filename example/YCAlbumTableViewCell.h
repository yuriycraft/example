//
//  YCAlbumTableViewCell.h
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCAlbumTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *photoCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *albumNameLabel;

@end
