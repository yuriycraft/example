//
//  YCPhotoModel.h
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCPhotoModel : NSObject

@property(strong,nonatomic)NSString *album_id;
@property(strong,nonatomic)NSDate * date;
@property(strong,nonatomic)NSNumber * height;
@property(strong,nonatomic)NSNumber * width;
@property(strong,nonatomic)NSString * photo_id;
@property(strong,nonatomic)NSString * owner_id;

@property(strong,nonatomic)NSString *photo_130;
@property(strong,nonatomic)NSString *photo_604;
@property(strong,nonatomic)NSString *photo_75;
@property(strong,nonatomic)NSString *text;

-(id)initWithRequest:(NSDictionary*) request;


#warning unComment
@property(nonatomic, strong, readwrite) NSNumber *selectedStatus;
@end
