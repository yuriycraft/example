//
//  AppDelegate.h
//  example
//
//  Created by Dmitriy on 24.03.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

