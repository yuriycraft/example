//
//  YCPhotoCollectionViewCell.h
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>



#warning unComment

@class YCPhotoCollectionViewCell;

@protocol YCPhotoCollectionViewCellDelegate <NSObject>

-(void)setSelectedItemWithCell:(YCPhotoCollectionViewCell*)cell;

@end

typedef void (^YCPhotoCollectionViewCellSelecteBlock)(NSNumber *selectedStatus);





@interface YCPhotoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property(assign,nonatomic) CGSize imageSize;



#warning unComment
@property(nonatomic, strong, readwrite) YCPhotoCollectionViewCellSelecteBlock selectBlock;
@property (nonatomic, strong, readwrite) NSNumber *selectedStatus;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property(nonatomic, weak, readwrite) id <YCPhotoCollectionViewCellDelegate> cellDelegate;

- (void)buttonSelectBlock:(void (^)(NSNumber *selectedStatus))selectBlock;


@end
