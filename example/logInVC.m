//
//  logInVC.m
//  VK Feed
//
//  Created by Dmitriy on 17.08.15.
//  Copyright (c) 2015 Dima. All rights reserved.
//

#import "logInVC.h"
#import "AppDelegate.h"

@interface logInVC ()

@end

@implementation logInVC
@synthesize vkAppLogin, vkPassLogin, logoImageView, backgroundImageView ;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setDesign];
    scope = [NSArray arrayWithObjects:@"wall",@"photos",@"users",@"video",@"friends",@"messages",@"notify",@"offline",@"groups",@"audio", nil];
    [VKSdk initializeWithDelegate:self andAppId:@"4822145" apiVersion:@"5.21"];
    [self updateUserData];

}

-(void)updateUserData{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"]) {
        NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"];
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        
        NSDictionary *param = @{@"user_ids":userId,
                                @"fields": @"sex, bdate, city, country, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, photo_id, online, online_mobile, domain, has_mobile, relation, screen_name, occupation",
                                @"access_token" : token
                                
                                };
        
        VKRequest *usersGet = [VKRequest requestWithMethod:@"users.get" andParameters:param andHttpMethod:@"POST"];
        [usersGet setPreferredLang:@"ru"];
        usersGet.attempts = 3;
        
        [usersGet executeWithResultBlock:^(VKResponse * response) {
            
            NSMutableArray *posts_array = response.json ;
            NSDictionary *user = posts_array[0];
            
            [[NSUserDefaults standardUserDefaults] setObject:user forKey:@"user"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        } errorBlock:^(NSError * error) {
            if (error.code != VK_API_ERROR) {
                [error.vkError.request repeat];
            }
            else {
                NSLog(@"VK error: %@", error);
            }
        }];
    }
}


-(void)setDesign{
    
    vkPassLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    vkPassLogin.layer.borderWidth = 0.3f;
    vkPassLogin.layer.cornerRadius = 4;
    
    vkAppLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    vkAppLogin.layer.borderWidth = 0.3f;
    vkAppLogin.layer.cornerRadius = 4;
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self clearCookie];
    
    [[self navigationController] setNavigationBarHidden: YES animated:NO];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"logIn"] boolValue] == FALSE) {
        vkAppLogin.hidden = NO ;
        vkPassLogin.hidden = NO ;
        logoImageView.hidden = NO ;
        backgroundImageView.hidden = NO ;
    }else{
        [self performSegueWithIdentifier:@"main" sender:nil];
        vkAppLogin.hidden = YES ;
        vkPassLogin.hidden = YES ;
        logoImageView.hidden = YES ;
        backgroundImageView.hidden = YES ;
    }
    
    if (![VKSdk vkAppMayExists]) {
        vkAppLogin.enabled = NO;
    }else{
        vkAppLogin.enabled = YES;
    }
    
}

- (IBAction)vkAppLogin:(id)sender {
    if ([VKSdk vkAppMayExists]) {
        [VKSdk authorize:scope revokeAccess:YES];
    }
}

- (IBAction)webViewLogin:(id)sender {
    [VKSdk authorize:scope revokeAccess:YES forceOAuth:YES inApp:YES];
}

#pragma mark - vk_delegate

-(void)vkSdkReceivedNewToken:(VKAccessToken *)newToken{
    [self updateUserAccounts:newToken];
}

- (void)vkSdkRenewedToken:(VKAccessToken *)newToken{
    [self updateUserAccounts:newToken];
}

-(void)updateUserAccounts:(VKAccessToken *) newToken{
    
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"logIn"];
    [[NSUserDefaults standardUserDefaults] setObject:newToken.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setObject:newToken.accessToken forKey:@"access_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"main" sender:nil];
    [self updateUserData];
    
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller{
    [[self getTopMostViewController] presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkDidDismissViewController:(UIViewController *)controller{

}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError{
    
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    
    if (visibleVC) {
        if (![[self getTopMostViewController] isKindOfClass:[VKCaptchaViewController class]]) {
            visibleVC = [self getTopMostViewController];
        }
    }else{
        visibleVC = [self getTopMostViewController];
    }
    
    [vc presentIn:visibleVC];
}

-(void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken{
    NSLog(@"expiredToken - %@", expiredToken);
}

-(void)vkSdkUserDeniedAccess:(VKError *)authorizationError{
    NSLog(@"authorizationError - %@", authorizationError);
}

#pragma mark - получение текущего (видимого) контроллера

- (UIViewController*) getTopMostViewController
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        
        //added this block of code for iOS 8 which puts a UITransitionView in between the UIWindow and the UILayoutContainerView
        if ([responder isEqual:window])
        {
            //this is a UITransitionView
            if ([[subView subviews] count])
            {
                UIView *subSubView = [subView subviews][0]; //this should be the UILayoutContainerView
                responder = [subSubView nextResponder];
            }
        }
        
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topViewController: (UIViewController *) responder];
        }
    }
    
    return nil;
}

- (UIViewController *) topViewController: (UIViewController *) controller
{
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(void)clearCookie{
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"vk.com"];
        
        if(domainRange.length > 0) {
            [storage deleteCookie:cookie];
        }
    }
}


@end
