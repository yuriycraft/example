//
//  YCPhotoCollectionViewCell.m
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCPhotoCollectionViewCell.h"

@implementation YCPhotoCollectionViewCell

#warning unComment
#pragma Action

- (void)setSelectedStatus:(NSNumber *)selectedStatus {
    
    if ([selectedStatus boolValue]) {
        
        [_selectButton setSelected:YES];
        
        self.image.alpha = 0.9f;
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             _selectButton.transform = CGAffineTransformMakeScale(0.9, 0.9);
                             _selectButton.transform = CGAffineTransformMakeScale(1, 1);
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.1
                                              animations:^{
                                                  _selectButton.transform =
                                                  CGAffineTransformMakeScale(0.9, 0.9);
                                                  _selectButton.transform =
                                                  CGAffineTransformMakeScale(1, 1);
                                                  
                                              }];
                         }];
        
    } else  {
        
        [_selectButton setSelected:NO];
        self.image.alpha = 1.0f;
        [UIView animateWithDuration:0.1
                         animations:^{
                             _selectButton.transform = _selectButton.transform =
                             CGAffineTransformMakeScale(0.9, 0.9);
                             CGAffineTransformMakeScale(1, 1);
                         }
                         completion:^(BOOL finished) {
                             [UIView animateWithDuration:0.1
                                              animations:^{
                                                  _selectButton.transform = _selectButton.transform =
                                                   CGAffineTransformMakeScale(0.9, 0.9);
                                                  CGAffineTransformMakeScale(1, 1);
                                                  
                                              }];
                         }];
    }
}

- (void)buttonSelectBlock:(void (^)(NSNumber *seletedStatus))selectBlock {
    NSLog(@"SELCT  = %@",selectBlock);
    _selectBlock = selectBlock;
}

- (IBAction)selectButtonAction:(id)sender {
    
    [_cellDelegate setSelectedItemWithCell:self];
}


@end
