//
//  ViewController.h
//  example
//
//  Created by Dmitriy on 24.03.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)presentAddPost:(id)sender;

@end

