//
//  YCPhotoTableViewCell.m
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCPhotoTableViewCell.h"
#import "YCPhotoCollectionViewCell.h"

@implementation YCPhotoCollectionView

@end


@implementation YCPhotoTableViewCell



-(void)awakeFromNib {
    [super awakeFromNib];
    self.isOverCountPhoto = NO;
    self.pageIndexPhotos = 0;
    
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPathSection:(NSInteger )indexPathSection
{
    
    self.myCollectionView.dataSource = dataSourceDelegate;
    self.myCollectionView.delegate = dataSourceDelegate;
 
    [self.myCollectionView setTag:indexPathSection];
  
    [self.myCollectionView reloadData];
}










@end
