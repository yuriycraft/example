//
//  YCPhotoModel.m
//  example
//
//  Created by book on 19.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import "YCPhotoModel.h"

@implementation YCPhotoModel

-(id)initWithRequest:(NSDictionary*) request {

    self = [super init];
    self.album_id = request[@"album_id"];
    self.date = request[@"date"];
    self.height = request[@"height"];
    self.width = request[@"width"];
    self.photo_id = request[@"id"];
    self.owner_id = request[@"owner_id"];
    self.photo_130 = request[@"photo_130"];
    self.photo_604 = request[@"photo_604"];
    self.photo_75 = request[@"photo_75"];
    self.text = request[@"text"];
    
    return self;
    
}

@end
