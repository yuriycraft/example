//
//  YCPhotoTableViewCell.h
//  example
//
//  Created by book on 18.06.16.
//  Copyright © 2016 Dmitriy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCPhotoCollectionView : UICollectionView



@end

static NSString *CollectionViewCellIdentifier = @"CollectionViewCellIdentifier";


@interface YCPhotoTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet YCPhotoCollectionView *myCollectionView;
@property (nonatomic, strong) NSArray *sourceArray;
@property(assign,nonatomic)BOOL isOverCountPhoto;
@property(assign,nonatomic)NSInteger pageIndexPhotos;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPathSection:(NSInteger )indexPathSection ;

@end
